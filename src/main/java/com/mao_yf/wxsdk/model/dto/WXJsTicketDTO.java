package com.mao_yf.wxsdk.model.dto;

import lombok.Data;

/**
 * myf
 * 2020/8/10 0010 上午 10:13
 */
@Data
public class WXJsTicketDTO {

    private int errcode;

    private String errmsg;

    private String ticket;

    private String expiresIn;
}
