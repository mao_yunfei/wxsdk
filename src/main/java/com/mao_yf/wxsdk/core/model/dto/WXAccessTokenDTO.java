package com.mao_yf.wxsdk.core.model.dto;

import lombok.Data;

/**
 * myf
 * 2020/8/10 0010 上午 9:46
 */
@Data
public class WXAccessTokenDTO {

    private String accessToken;

    private Long expiresIn;

    private Long errcode;

    private String errmsg;
}
