package com.mao_yf.wxsdk.core.servicel.impl;

import com.mao_yf.wxsdk.core.servicel.WXSDKService;
import com.mao_yf.wxsdk.core.utils.WXSDKUtils;
import com.mao_yf.wxsdk.core.utils.WXSignUtils;
import com.mao_yf.wxsdk.ms.constants.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * myf
 * 2020/8/10 0010 上午 9:26
 */
@Slf4j
@Service
public class WXSDKServiceImpl implements WXSDKService {

    @Resource
    private WXSDKUtils wxsdkUtils;

    @Override
    public Result getConfig(String appId, String url) {
        Result result;
        try {
            String jsTicket = wxsdkUtils.getJsTicket();
            if (StringUtils.isEmpty(jsTicket)) {
                return Result.failure();
            }
            Map<String, String> sign = WXSignUtils.sign(jsTicket, url);
            sign.put("appId", appId);
            result = Result.success(sign);
        } catch (Exception e) {
            log.error("@@@@getConfig-异常", e);
            result = Result.failure();
        }
        return result;
    }
}
