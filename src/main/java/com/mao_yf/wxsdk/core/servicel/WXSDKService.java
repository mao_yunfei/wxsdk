package com.mao_yf.wxsdk.core.servicel;


import com.mao_yf.wxsdk.ms.constants.Result;

/**
 * myf
 * 2020/8/10 0010 上午 9:26
 */
public interface WXSDKService {

    /**
     * 获取微信网页jsTicket
     *
     * @param appId 公众号id
     * @param url   网页地址
     * @return
     */
    Result getConfig(String appId, String url);
}
