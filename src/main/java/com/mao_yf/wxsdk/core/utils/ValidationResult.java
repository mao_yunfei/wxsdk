package com.mao_yf.wxsdk.core.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.Map;


public class ValidationResult {
	
	@Setter
	private Map<String, String> errorMsg;
	
	private Boolean hasErrors = false;

	public Boolean getHasErrors() {
		if(CollectionUtils.isEmpty(errorMsg)) {
			hasErrors = false;
		}else {
			hasErrors = true;
			
		}
		return hasErrors;
	}

	public String getErrorMsg() {
		return JSONObject.toJSONString(errorMsg);
	}
	
}
