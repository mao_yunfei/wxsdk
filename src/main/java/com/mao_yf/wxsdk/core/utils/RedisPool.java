package com.mao_yf.wxsdk.core.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * redis连接池
 */
@Component
public class RedisPool {

    @Value("${private.redis.host}")
    private String host;
    @Value("${private.redis.password}")
    private String password;
    @Value("${private.redis.port}")
    private Integer port;

    private JedisPool jedisPool;

    public synchronized JedisPool getJedisPool() {
        if (jedisPool == null) {
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(100);
            config.setMaxIdle(8);
            config.setMaxWaitMillis(1000 * 10);
            //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
            config.setTestOnBorrow(true);
            //new JedisPool(config, ADDR, PORT, TIMEOUT, AUTH);
            jedisPool = new JedisPool(config, host, port, 10000, password);
        }

        return jedisPool;

    }

    public synchronized Jedis getResource() {
        synchronized (RedisPool.class) {

            if (jedisPool == null) {
                jedisPool = getJedisPool();
            }
            return jedisPool.getResource();
        }

    }

    public RedisPool() {
    }

    public synchronized void returnResource(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }

    }


}
