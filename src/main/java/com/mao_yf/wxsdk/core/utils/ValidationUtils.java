package com.mao_yf.wxsdk.core.utils;


import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 校验工具类
 * 
 */
public class ValidationUtils {

	private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	public static <T> ValidationResult validateBean(T obj) {
		return validateBean(obj,Default.class);
	}

	/**
	 * 自定义分组 bean 属性验证
	 * @param obj bean
	 * @param groups 分组验证接口
	 * @param <T>
	 * @return
	 */
	public static <T> ValidationResult validateBean(T obj, Class<?>... groups) {
		ValidationResult result = new ValidationResult();
		Set<ConstraintViolation<T>> set = validator.validate(obj, groups);
		if (!CollectionUtils.isEmpty(set)) {
			Map<String, String> errorMsg = new HashMap<>(set.size());
			for (ConstraintViolation<T> cv : set) {
				errorMsg.put(cv.getPropertyPath().toString(), cv.getMessage());
			}
			result.setErrorMsg(errorMsg);
		}
		return result;
	}


	public static <T> ValidationResult validateProperty(T obj, String propertyName) {
		return validateProperty(obj, propertyName, Default.class);
	}


	public static <T> ValidationResult validateProperty(T obj, String propertyName, Class<?>... groups) {
		ValidationResult result = new ValidationResult();
		Set<ConstraintViolation<T>> set = validator.validateProperty(obj, propertyName, groups);
		if (!CollectionUtils.isEmpty(set)) {
			Map<String, String> errorMsg = new HashMap<>(set.size());
			for (ConstraintViolation<T> cv : set) {
				errorMsg.put(propertyName, cv.getMessage());
			}
			result.setErrorMsg(errorMsg);
		}
		return result;
	}
}