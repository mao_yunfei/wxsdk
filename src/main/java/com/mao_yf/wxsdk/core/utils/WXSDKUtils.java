package com.mao_yf.wxsdk.core.utils;

import com.alibaba.fastjson.JSON;
import com.mao_yf.wxsdk.core.model.dto.WXAccessTokenDTO;
import com.mao_yf.wxsdk.core.model.dto.WXJsTicketDTO;
import com.mao_yf.wxsdk.ms.constants.ProjectConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;

/**
 * myf
 * 2020/8/10 0010 上午 9:33
 */
@Slf4j
@Component
public class WXSDKUtils {

    @Resource
    private RedisPool redisPool;

    @Value("${private.wechat.appId}")
    private String appId;

    @Value("${private.wechat.appSecret}")
    private String appSecret;

    public String getAccessToken() {
        String accessToken = "";
        synchronized (WXSDKUtils.class) {
            try (Jedis jedis = redisPool.getResource()) {
                accessToken = jedis.get(ProjectConstant.WX_ACCESS_TOKEN);
                log.info("@@@@getAccessToken-redisValue={}", accessToken);
                if (StringUtils.isEmpty(accessToken)) {
                    //拼接请求路径
                    String getTokenUrl = String.format(ProjectConstant.GET_ACCESS_TOKEN_URL, appId, appSecret);
                    String resultStr = HttpTool.nGet(getTokenUrl, null);
                    log.info("@@@@获取微信accessToken结果-{}", resultStr);
                    //解析响应结果
                    WXAccessTokenDTO wxAccessTokenDTO = JSON.parseObject(resultStr, WXAccessTokenDTO.class);
                    accessToken = wxAccessTokenDTO.getAccessToken();
                    if (!ObjectUtils.isEmpty(wxAccessTokenDTO.getErrcode())) {
                        throw new RuntimeException(JSON.toJSONString(wxAccessTokenDTO));
                    }
                    //设置7000秒的有效时间
                    jedis.setex(ProjectConstant.WX_ACCESS_TOKEN, 7000, accessToken);
                }
            } catch (Exception e) {
                log.error("@@@@获取微信accessToken-异常", e);
            }
        }
        return accessToken;
    }

    public String getJsTicket() {
        String jsTicket = "";
        synchronized (WXSDKUtils.class) {
            try (Jedis jedis = redisPool.getResource()) {
                jsTicket = jedis.get(ProjectConstant.WX_JS_TICKET);
                log.info("@@@@获取jsTicket-redisValue={}", jsTicket);
                if (StringUtils.isEmpty(jsTicket)) {
                    //获取接口凭据token
                    String accessToken = getAccessToken();
                    if (StringUtils.isEmpty(accessToken)) {
                        throw new RuntimeException("获取accessToken为空");
                    }
                    //拼接请求路径
                    String getJsTicketUrl = String.format(ProjectConstant.GET_JS_TICKET_URL, accessToken);
                    log.info("@@@@获取jsTicket-getJsTicketUrl={}", getJsTicketUrl);
                    String resultStr = HttpTool.nGet(getJsTicketUrl, null);
                    log.info("@@@@获取微信jsTicket结果-{}", resultStr);
                    //解析响应结果
                    WXJsTicketDTO wxJsTicketDTO = JSON.parseObject(resultStr, WXJsTicketDTO.class);
                    log.info("@@WXJsTicketDTO={}", wxJsTicketDTO);
                    //42001:token过期,40001:token无效(可能被重置)
                    if (wxJsTicketDTO.getErrcode() == 42001 || wxJsTicketDTO.getErrcode() == 40001) {
                        clearAccessToken();
                        accessToken = getAccessToken();
                        if (StringUtils.isEmpty(accessToken)) {
                            throw new RuntimeException("获取accessToken为空");
                        }
                        getJsTicketUrl = String.format(ProjectConstant.GET_JS_TICKET_URL, accessToken);
                        log.info("@@@@获取jsTicket-getJsTicketUrl={}", getJsTicketUrl);
                        resultStr = HttpTool.nGet(getJsTicketUrl, null);
                        log.info("@@@@获取微信jsTicket结果-{}", resultStr);
                        //解析响应结果
                        wxJsTicketDTO = JSON.parseObject(resultStr, WXJsTicketDTO.class);
                        log.info("@@WXJsTicketDTO={}", wxJsTicketDTO);
                        if (wxJsTicketDTO.getErrcode() != 0) {
                            throw new RuntimeException(JSON.toJSONString(wxJsTicketDTO));
                        }
                    }
                    jsTicket = wxJsTicketDTO.getTicket();
                    //设置7000秒的有效时间
                    jedis.setex(ProjectConstant.WX_JS_TICKET, 7000, jsTicket);
                }
            } catch (Exception e) {
                log.error("@@@@获取微信jsTicket-异常", e);
            }
        }
        return jsTicket;
    }

    public void clearAccessToken() {
        try (Jedis jedis = redisPool.getResource()) {
            Long delResult = jedis.del(ProjectConstant.WX_ACCESS_TOKEN);
            log.info("@@@@删除accessToken-结果:{}", delResult);
        } catch (Exception e) {
            log.info("@@@@删除accessToken-异常");
        }
    }

    public void clearJsTicket() {
        try (Jedis jedis = redisPool.getResource()) {
            Long delResult = jedis.del(ProjectConstant.WX_JS_TICKET);
            log.info("@@@@删除jsTicket-结果:{}", delResult);
        } catch (Exception e) {
            log.info("@@@@删除jsTicket-异常");
        }
    }
}
