package com.mao_yf.wxsdk.controller;

import com.mao_yf.wxsdk.core.servicel.WXSDKService;
import com.mao_yf.wxsdk.ms.constants.Result;
import com.mao_yf.wxsdk.ms.constants.ResultEntity;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * myf
 * 2020/8/7 0007 下午 4:40
 */
@Slf4j
@RestController
public class WXSDKController {

    @Value("${private.wechat.appId}")
    private String appId;

    @Resource
    private WXSDKService wxsdkService;

    @ApiOperation("获取jsTicket")
    @RequestMapping(value = "/v1/get/config", method = {RequestMethod.GET, RequestMethod.POST})
    public Result getConfig(String url) {
        Result result;
        log.info("@@@@getConfig-入参-url={}", url);
        if (StringUtils.isEmpty(url)) {
            result = Result.genResult(ResultEntity.PARAM_IS_EMPTY);
        } else {
            result = wxsdkService.getConfig(appId, url);
        }
        log.info("@@@@getConfig-出参-{}", result);
        return result;
    }

}
