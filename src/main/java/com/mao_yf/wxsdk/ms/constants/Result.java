package com.mao_yf.wxsdk.ms.constants;



import com.alibaba.fastjson.JSON;

import java.io.Serializable;

/** * 统一API响应结果封装 * 新增对应枚举值的封装 */
public class Result<T> implements Serializable {
	/** * 返回的code */
	private int code;
	/** * 返回的文言 */
	private String message;
	/** * 返回的数值 */
	private T data;


	public int getCode() {
		return code;
	}

	public Result<T> setCode(int resultCode) {
		this.code = resultCode;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public Result<T> setMessage(String message) {
		this.message = message;
		return this;
	}

	public T getData() {
		if(data == null){
			return (T) new Object();
		}
		return data;
	}

	public Result<T> setData(T data) {
		this.data = data;
		return this;
	}



	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	public static <T> Result<T> success() {
		return new Result<T>().setCode(ResultEntity.SUCCESS.getCode()).setMessage(ResultEntity.SUCCESS.getMsg());
	}

	public static <T> Result<T> success(T data) {
		return new Result<T>().setCode(ResultEntity.SUCCESS.getCode()).setMessage(ResultEntity.SUCCESS.getMsg())
				.setData(data);
	}
	public static <T> Result<T> success(ResultEntity resultEntity) {
		return new Result<T>().setCode(resultEntity.getCode()).setMessage(resultEntity.getMsg());
	}

	/**
	 * 默认失败返回 code ：FAILURE
	 *
	 * @return
	 */
	public static <T> Result<T> failure() {
		return new Result<T>().setCode(ResultEntity.FAIL.getCode()).setMessage(ResultEntity.FAIL.getMsg());
	}

	/**
	 * 自定义失败返回
	 *
	 * @return
	 */
	public static <T> Result<T> failure(ResultEntity resultEntity) {
		return new Result<T>().setCode(resultEntity.getCode()).setMessage(resultEntity.getMsg());
	}

	/**
	 * 默认失败返回 code ：EXCIPTION
	 *
	 * @return
	 */
	public static <T> Result<T> exception() {
		return new Result<T>().setCode(ResultEntity.FAIL.getCode()).setMessage(ResultEntity.FAIL.getMsg());
	}

	/**
	 * 自定义code和message
	 * @return
	 */
	public static <T> Result<T> genResult(int code, String message){
		return new Result<T>()
				.setCode(code)
				.setMessage(message);
	}
	/**
	 * 自定义code和message
	 * @return
	 */
	public static <T> Result<T> genResult(ResultEntity resultEntity ){
		return new Result<T>()
				.setCode(resultEntity.getCode())
				.setMessage(resultEntity.getMsg());
	}
}
