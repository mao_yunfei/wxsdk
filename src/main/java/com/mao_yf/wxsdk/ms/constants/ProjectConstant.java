package com.mao_yf.wxsdk.ms.constants;


/**
 * 项目基础路径
 */
public class ProjectConstant {

    /**
     * 微信公众号access_token有效期2小时
     */
    public final static String WX_ACCESS_TOKEN = "WXSDK:WX:ACCESS:TOKEN";

    /**
     * 微信公众号js_ticket有效期2小时
     */
    public final static String WX_JS_TICKET = "WXSDK:WX:JS:TICKET";

    /**
     * 获取微信token
     */
    public final static String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

    /**
     * 获取JsTicket
     */
    public final static String GET_JS_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi";
}
