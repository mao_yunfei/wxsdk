package com.mao_yf.wxsdk.ms.constants;

public enum ResultEntity {

    SUCCESS(200, "操作成功!"),//成功
    FAIL(90004, "操作失败！"),//连接失败
    PARAM_IS_EMPTY(91004, "缺少参数"),
    PARAM_FORMAT_ERROR(91005, "参数格式错误！"),

    //新项目end
    VALIDATION_FAILED(400, "验证失败"),//验证失败
    SERVER_ERROR(90000, "缓存服务异常错误"),//系统错误
    SERVER_REDIS_ERROR(90001, "redis服务不可用"),
    SERVER_NOT_FOUND(92002, "接口不存在，请联系管理员！"),
    UNKNOWN_EXCEPTION(93003, "未知异常"),//未知异常


    END(99999, "结束！");

    ResultEntity(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private final int code;

    private final String msg;


    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
